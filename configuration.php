<?php


// Google API Preferences -- You MUST change these
// Your app must also have Google Books API and Google Drive API available.
$client_id = '';
$client_secret = '';
$redirect_uri = '';

// URL to your CalibrePHP Install (ex. http://localhost/library without trailing slash)
$books_url = '';