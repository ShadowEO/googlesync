# ABOUT #

An adapter for CalibrePHP which requires a one-line mod to ImageHelper.php in /app/Views/Helper/ in your CalibrePHP Approot.

This adapter allows you to add a Send to Google Books button to the download dropdown which will connect to the user's Google Drive account and then send the book to Google Play Books.

You do need a Google Developers Key with the following services allowed in order to use this adapter:

Google Books and Google Drive


NOTE: I do realize that my client secret was exposed here, I have already reset the secret in the Google Developers panel. Ignore those commits, this was at one point a private repository.

# INSTALL #

Simply clone it to a folder that is not within your CalibrePHP app-root, and apply the patch which will be made available soon to ImageHelper in CalibrePHP/app/Views/Helper/

Open configuration.php and enter the information requested, you can find this information in your Google Developers Console after creating the project. Ensure you have the correct APIs enabled.

# HOW DOES THIS WORK? #

Well, when you add the link using the patch, it adds a new option to the Download menu which says Google Play Books ($format), if you select that, you are then redirected to the Google login screen where you are asked to provide your login details, at this point you should be redirected back to GoogleSync.php which will download the epub from CalibrePHP, then push it to the user's Google Drive account, and from there, to Google Play Books via the Cloudloading API.

# FAQ #

Q: Why does this need access to a user's Google Drive account?
A: The undocumented cloudloading API only allows pulling files from Google Drive. Unfortunately, also due to this restriction, I also cannot have the project "auto-remove" cloudloaded books from the user's Drive account, I had experimented around with that, however the script returns before the cloudloading is finished, meaning that the book is deleted from Drive before Google Books has had a chance to process it resulting in Google Books throwing an error. Perhaps there will be a way around this in the future, but it seems unlikely.

Q: Does this do anything else?
A: Sure! It can serve as an example of how to upload files to Google Drive complete with Metadata including Title, Description, Mimetype, etc. As well as a small example of how to use the currently undocumented Google Books Cloudloading API. Other than that... No...

Q: Can I have this redirect back to the previous page once finished, or can I have some sort of "done" page?
A: Yes, the script accepts escaped URLs through the prev_url GET parameter. If this parameter does not exist, it will attempt to load the file: done.php, this file can do anything you wish, including informing the user that their book should appear shortly and then redirects them back to CalibrePHP.

Q: Do you support "such and such library web interface"
A: Technically, due to the way this script runs... Yes. It can be modified to grab ePubs/PDFs from any web interface that provides a direct download link. Do I personally support it? No, Sorry.