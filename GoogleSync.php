<?php

/** ShadowSync for Google Play Books **
 * 
 * @author Toxus
 * @description Allows developers to call to the page with information about an ePub or compatible file
 * and sends it to Google Play Books by proxying off the user's Google Drive.
 * 
 **/
 
session_start();
set_include_path("./lib/".PATH_SEPARATOR.get_include_path());
set_include_path("./lib/Google/src/".PATH_SEPARATOR.get_include_path());
require_once 'Google/src/Google/Client.php';
require_once 'Google/src/Google/Http/MediaFileUpload.php';
require_once 'Google/src/Google/Service/Drive.php';
require_once 'Google/src/Google/Service/Books.php';
require_once 'ePubMeta/epub.php';
require_once 'ePubMeta/util.php';
//$UserDatabase = new SQLite3('/media/Storage/System/library-auth.db');

include("configuration.php");

if($_REQUEST['book'] && $_REQUEST['format'] && !isset($_SESSION['mimetype']))
{
    
    switch(strtolower($_REQUEST['format']))
    {
        case 'epub':
            $_SESSION['mimetype'] = "application/epub+zip";
            break;
        case 'mobi':
            $_SESSION['mimetype'] = "application/x-mobipocket-ebook";
            die("Mobi is not a supported filetype by Google Play Books cloudloading.");
            break;
        case 'pdf':
            $_SESSION['mimetype'] = "application/pdf";
            break;
    }
}

$client = new Google_Client();
$client->setClientId($client_id);
$client->setClientSecret($client_secret);
$client->setRedirectUri($redirect_uri);
$client->addScope("https://www.googleapis.com/auth/drive");
$client->addScope("https://www.googleapis.com/auth/books");

$drive = new Google_Service_Drive($client);
$books = new Google_Service_Books($client);

if(isset($_REQUEST['from_url']))
{
    $_SESSION['prev_url'] = $_REQUEST['from_url'];
}

if(isset($_REQUEST['book']))
{
    $_SESSION['book'] = $_REQUEST['book'];
    $_SESSION['format'] = strtolower($_REQUEST['format']);
}

if(isset($_REQUEST['dologin']))
{
    include("login.php");
}

if(isset($_REQUEST['logout'])) {
    unset($_SESSION['token']);
}

if(isset($_GET['code'])) {
    $client->authenticate($_GET['code']);
    $_SESSION['token'] = $client->getAccessToken();
    
    header('Location: '.$redirect_uri);
    exit;
}

if(isset($_SESSION['token']) && $_SESSION['token']) {
    $client->setAccessToken($_SESSION['token']);
    //if(isset($_SESSION['from_url']) && isset($_SESSION['UserData']['UserID'])) {
    //   $db->query("UPDATE Users SET google_access_token = '".$_SESSION['token']."' WHERE UserID = '".$_SESSION['UserData']['UserID']."';");
    //    header('Location: '. filter_var($_SESSION['from_url'], FILTER_SANITIZE_URL));
    //}
    if($client->isAccessTokenExpired()) {
        unset($_SESSION['token']);
    }
} else {
    $authUrl = $client->createAuthUrl();
    header("Location: ".$authUrl);
    exit;
}

if($client->getAccessToken()) {
    // Download the file to the temporary folder before we upload it.
    file_put_contents("/tmp/".session_id()."-eBookUpload.".strtolower($_SESSION['format']), file_get_contents($books_url."/books/download/".$_SESSION['book']."/".$_SESSION['format']));
    $file = new Google_Service_Drive_DriveFile();
    $file->title = "$_SESSION[book].$_SESSION[format]";
 /**
    if($_SESSION['format'] == strtolower("epub"))
    {
        try {
            $epub = new EPub();
        } catch (Exception $e){
            $error = $e->getMessage();
        }
        $file->description = $epub->Title() . " by " . $epub->Author();
    } else {
        
    }
    */
    $file->description = "Temporary eBook Upload";
    $file->setMimeType($_SESSION['mimetype']);
    $filesize = filesize("/tmp/".session_id()."-eBookUpload.".strtolower($_SESSION['format']));
    $client->setDefer(false);
    try {
        $request = $drive->files->insert($file, array('data' => file_get_contents("/tmp/".session_id()."-eBookUpload.".$_SESSION['format']),'mimeType' => $_SESSION['mimetype'], 'uploadType' => 'media'));    
    } catch (Exception $e) {
    print "An error occurred adding the file to Google Drive: " . $e->getMessage();
    exit;
  }  
    unlink("/tmp/".session_id()."-eBookUpload.".strtolower($_SESSION['format']));
    $eBookFileID = $request->getId();
    try {
      $books->cloudloading->addBook(array("drive_document_id"=>$eBookFileID,
      "mime_type"=>$_SESSION['mimetype']));  
    } catch (Exception $e) {
        print "An error occurred adding the book to Google Books: " .$e->getMessage();
        exit;
    }
    //$drive->files->delete($eBookFileID);
    //include("done.php");
    unset($_SESSION['format']);
    unset($_SESSION['book']);
    if($_SESSION['prev_url'])
    {
      header("Location: ".urldecode($_SESSION['prev_url']));
      unset($_SESSION['prev_url']);  
      exit;
    } else {
        include("done.php");
        exit;
    }
    
}
?>    

