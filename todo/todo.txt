(A) 2014-08-19 Create registration page to save new user account and Auth Token +ShadowSync @user-management
(A) 2014-08-19 Create main account panel which shows recent synchronizations and allows the user to delete their account if needed. +ShadowSync @user-management
(B) 2014-08-19 Integrate ShadowSync with a Library application such as BicBucStriim +ShadowSync @integration
(C) 2014-08-19 Create a "Send to Google Play Books" button for ShadowSync +ShadowSync @integration @beautification
